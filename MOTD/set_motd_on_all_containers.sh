#!/bin/bash

CONTAINERS=`vzlist -a | sed '1d' | awk '{print $1}'`
for C in $CONTAINERS; do
	ETC=/vz/private/$C/etc/
	MOTD_CFG_DIR=/vz/private/$C/etc/update-motd.d/
	SSH_CFG=/vz/private/$C/etc/ssh/sshd_config

	if [ ! -f $SSH_CFG ]; then
		continue
	fi

	cp $SSH_CFG $SSH_CFG.backup
	grep -Ev '^(PrintLastLog|PrintMotd)' > $SSH_CFG <  $SSH_CFG.backup
	echo >> $SSH_CFG
	echo 'PrintLastLog no' >> $SSH_CFG
	echo 'PrintMotd yes' >> $SSH_CFG
	rm $SSH_CFG.backup

	if [ -f motd.art ]; then
		/bin/cp -f motd.art $ETC
	fi

	for F in $(ls $MOTD_CFG_DIR); do
		FILE=$MOTD_CFG_DIR
		FILE+=$F
		chmod -x $FILE
	done

	if [ -f 00-header ]; then
		/bin/cp -f 00-header $MOTD_CFG_DIR
		FILE=$MOTD_CFG_DIR
		FILE+="00-header"
		chmod +x $FILE
	fi
done
