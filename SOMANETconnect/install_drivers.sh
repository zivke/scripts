#!/bin/bash

# SOMANETconnect - JTAG driver installation script

if [[ $EUID -ne 0 ]]; then
    printf "\nPlease run this script as root or using sudo.\n\n"
    exit 1;
fi

if [ $# -gt 1 ];
then
	printf "\nUsage: run \"sudo ./install_drivers\" to install JTAG drivers or \"sudo ./install_drivers remove\" to uninstall them\n\n";
	exit 1;
fi

if [ $# = 1 ];
then
	if [ $1 = "remove" ];
	then
		if [ ! -f /etc/udev/rules.d/99-xmos.rules ];
		then
			printf "\nNo JTAG drivers found\n\n";
			exit 1;
		fi
		while true; do
			read -p "Do you want to remove the JTAG drivers? (y/n - default yes) " answer
			answer=${answer:-y}
    		case $answer in
        		[Yy]* ) rm /etc/udev/rules.d/99-xmos.rules && printf "\nJTAG drivers successfully removed\n\n"; break;;
        		[Nn]* ) break;;
        		* ) echo "Please answer yes or no (y/n)";;
    		esac
    	done
    else
    	printf "\nUsage: run \"sudo ./install_drivers\" to install JTAG drivers or \"sudo ./install_drivers remove\" to uninstall them\n\n";
	fi
	exit 0;
fi

if [ -f /etc/udev/rules.d/99-xmos.rules ];
then
	while true; do
		read -p "Driver files already exist. Do you want to overwrite them? (y/n - default yes) " answer
		answer=${answer:-y}
    	case $answer in
        	[Yy]* ) cp drivers/99-xmos.rules /etc/udev/rules.d/ && printf "\nDrivers successfully overwritten\n\n"; break;;
        	[Nn]* ) break;;
        	* ) echo "Please answer yes or no (y/n)";;
    	esac
    done
else
	cp drivers/99-xmos.rules /etc/udev/rules.d/ && printf "\nDriver successfully installed\n\n"
fi
