#!/bin/bash
# Control example XC applications started by SOMANETconnect

if [ $# -gt 1 ] || [ $# -gt 1 ] || [ "$1" == "-?" ]; then
	printf "\nUsage:\n\t'./example_app_control.sh -s' to start the example XC application\n\t'./example_app_control.sh -t' to terminate the example XC application\n\n"
	exit 1
fi

XMOS_DEVICE_ID=0;
XSCOPE_PORT="127.0.0.1:10101";
EXAMPLE_APP_BINARY="example_binaries/xscopeAppExample.xe";

if [ $1 == "-s" ]; then
	#  Start the example XC application
	./xrun --id $XMOS_DEVICE_ID --io --xscope-realtime --xscope-port $XSCOPE_PORT $EXAMPLE_APP_BINARY
elif [ $1 == "-t" ]; then
	# Terminate all started xrun and xgdb processes for the "xscopeAppExample.xe" application
	XRUN_PROCESS_PIDS=`pgrep -f $EXAMPLE_APP_BINARY`
	for PROCESS in $XRUN_PROCESS_PIDS; do
		kill -SIGTERM $PROCESS
	done
else
	printf "\nUsage:\n\t'./example_app_control.sh -s' to start the example XC application\n\t'./example_app_control.sh -t' to terminate the example XC application\n\n"
fi