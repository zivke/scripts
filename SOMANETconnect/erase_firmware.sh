#!/bin/bash
# Erase the firmware from a SOMANET C22 or C21 device

if [ "$1" == "-?" ] || [ $# -ne 4 ] ; then
	printf "\nUsage:\n\t'./erase_firmware.sh -i 0 -t c22' to erase the firmware on a C22 device with the id 0\n\n"
	exit 1
fi

while getopts "i:t:" opt; do
    case "$opt" in
    i)  XMOS_DEVICE_ID=$OPTARG
        ;;
    t)  XMOS_DEVICE_TYPE=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

if [ $XMOS_DEVICE_TYPE == "c22" ]; then
	TARGET_FILE="targets/SOMANET-C22/SOMANET-C22.xn";
elif [ $XMOS_DEVICE_TYPE == "c21" ]; then
	TARGET_FILE="targets/SOMANET-C21-DX/SOMANET-C21-DX.xn";
else
	printf "\nError: Unsupported device type\n\n";
	exit 1;
fi

./xflash --id $XMOS_DEVICE_ID --erase-all --target-file $TARGET_FILE
