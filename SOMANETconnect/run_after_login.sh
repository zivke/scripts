#!/bin/bash

# Run SOMANETconnect after login

if [ $# -gt 1 ];
then
	printf "\nUsage: run \"./run_after_login.sh\" to automatically run SOMANETconnect after login or \"./run_after_login.sh remove\" to disable this option\n\n";
	exit 1;
fi

if [ $# = 1 ];
then
	if [ $1 = "remove" ];
	then
		if [ ! -f $HOME/.config/autostart/SOMANETconnect.desktop ];
		then
			printf "\nThis option has not been enabled yet\n\n";
			exit 1;
		fi
		while true; do
			read -p "Do you want to stop running SOMANETconnect automatically after login? (y/n - default yes) " answer
            answer=${answer:-y}
    		case $answer in
        		[Yy]* ) rm $HOME/.config/autostart/SOMANETconnect.desktop && printf "\nSOMANETconnect will no longer run automatically after login\n\n"; break;;
        		[Nn]* ) break;;
        		* ) echo "Please answer yes or no (y/n)";;
    		esac
    	done
    else
    	printf "\nUsage: run \"./run_after_login.sh\" to automatically run SOMANETconnect after login or \"./run_after_login.sh remove\" to disable this option\n\n";
	fi
	exit 0;
fi

if [ -f $HOME/.config/autostart/SOMANETconnect.desktop ];
then
    printf "\nSOMANETconnect has already been set to run after login.\n\n"
else
    while true; do
	   read -p "Do you want SOMANETconnect to run automatically after login? (y/n - default yes) " answer
       answer=${answer:-y}
        case $answer in
            [Yy]* ) SC_PATH=$PWD/SOMANETconnect;
                    mkdir -p $HOME/.config/autostart
                    printf "[Desktop Entry]\nType=Application\nName=SOMANETconnect\nPath=%s\nExec=%s --minimized\n" "$PWD" "$SC_PATH" > $HOME/.config/autostart/SOMANETconnect.desktop && printf "\nSOMANETconnect will be started after every login.\n\n";
        		  break;;
            [Nn]* ) break;;
            * ) echo "Please answer yes or no (y/n)";;
        esac
    done
fi
