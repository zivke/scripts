#!/bin/bash

printf "\n\n--------------------------------------------\nSOMANETconnect certificate import script\n--------------------------------------------\n"

if [[ $EUID == 0 ]]; then
    printf "\nThis script cannot be run as root or using sudo.\n\n\n"
    exit
fi

printf "\nWARNING: Please make sure that your browser is closed before continuing with certificate import.\n\n"

while true; do
    read -p "Do you want SOMANETconnect to import the needed certificates to your browser (Chrome and Firefox supported)? (y/n - default yes) " answer
    answer=${answer:-y}
    case $answer in
        [Yy]* ) printf "\nInstalling prerequisites...\n"
				
				command -v certutil >/dev/null 2>&1 || sudo apt-get -y install libnss3-tools >/dev/null 2>&1 || sudo yum -y install nss-tools >/dev/null 2>&1 || { echo ERROR: Cannot install the SOMANETconnect certificate. Please try importing it manually.; exit 1; }
                
                printf "\nPrerequisites installed\n\n"
                
                certificateFile="certificate/SOMANETconnect.crt"
				certificateName="127.0.0.1"

                while true; do
                    printf "Choose the browser to import the certificate into:\n1) Firefox\n2) Chrome\n3) both Firefox and Chrome (default)\n"
                    read answer2
                    answer2=${answer2:-3}
                    case $answer2 in
                        [1]* )
                                # Import the SOMANETconnect certificate into Firefox
				                for certDB in $(find  ~/.mozilla* -name "cert8.db")
				                do
                                    certDir=$(dirname ${certDB});
                                    certutil -A -n "${certificateName}" -t "TCu,Cuw,Tuw" -i ${certificateFile} -d ${certDir};
				                done
                                break;;
                        [2]* ) 
                                # Import the SOMANETconnect certificate into Chrome
                                certutil -d sql:$HOME/.pki/nssdb -A -t "TCu,Cuw,Tuw" -n ${certificateName} -i ${certificateFile};
                                break;;
                        [3]* )
                                # Import the SOMANETconnect certificate into Firefox
                                for certDB in $(find  ~/.mozilla* -name "cert8.db")
                                do
                                    certDir=$(dirname ${certDB});
                                    certutil -A -n "${certificateName}" -t "TCu,Cuw,Tuw" -i ${certificateFile} -d ${certDir};
                                done

                                # Import the SOMANETconnect certificate into Chrome
                                certutil -d sql:$HOME/.pki/nssdb -A -t "TCu,Cuw,Tuw" -n ${certificateName} -i ${certificateFile};

                                break;;
                        * ) echo "Please answer with 1, 2 or 3";;
                    esac
                done
                break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no (y/n)";;
    esac
done
