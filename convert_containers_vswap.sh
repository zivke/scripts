#!/bin/bash

if [ $# != 2 ]
then
	echo "Wrong number of parameters. Usage: \"./convert_containers_vswap FIRST_CTID LAST_CTID\"";
	exit 1;
fi

FIRST_CTID="$1"
LAST_CTID="$2"
RAM=256M
SWAP=512M

for (( CTID=$FIRST_CTID; CTID<=$LAST_CTID; CTID++ ))
do
	CFG=/etc/vz/conf/${CTID}.conf

	if [ ! -f $CFG ]; then
    	continue
	fi
	
	cp $CFG $CFG.pre-vswap
	grep -Ev '^(KMEMSIZE|LOCKEDPAGES|PRIVVMPAGES|SHMPAGES|NUMPROC|PHYSPAGES|VMGUARPAGES|OOMGUARPAGES|NUMTCPSOCK|NUMFLOCK|NUMPTY|NUMSIGINFO|TCPSNDBUF|TCPRCVBUF|OTHERSOCKBUF|DGRAMRCVBUF|NUMOTHERSOCK|DCACHESIZE|NUMFILE|AVNUMPROC|NUMIPTENT|ORIGIN_SAMPLE|SWAPPAGES)=' > $CFG <  $CFG.pre-vswap
	echo 'ORIGIN_SAMPLE="vswap-256m"' >> $CFG
	echo 'PRIVVMPAGES="unlimited"' >> $CFG
	vzctl set $CTID --ram $RAM --swap $SWAP --save
	vzctl set $CTID --reset_ub
done
