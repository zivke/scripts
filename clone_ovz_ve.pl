#!/usr/bin/perl
use Tie::File;

if(scalar(@ARGV) != 3) { die "Usage: perl clone_ovz_ve.pl old_ve first_new_ve how_many\n"; }
my $oldve = shift @ARGV; 
my $newve = shift @ARGV;
my $howmany = shift @ARGV;
my @iplist;
my $string;
my $i;
my $id = $newve - 100;

for($i=0; $i < $howmany; $i++) {

  print("Creating VE: $newve\n");
  `mkdir /vz/root/$newve`;
  `cp /etc/vz/conf/$oldve.conf /etc/vz/conf/$newve.conf`;
  `mkdir /vz/private/$newve`;
  `pushd /vz/private/$oldve; tar c --numeric-owner * | tar x --numeric-owner -C /vz/private/$newve`;

  my $file = "/etc/vz/conf/$newve.conf";
  tie @lines, 'Tie::File', $file or die "Can't open file\n";
  for(@lines) {
    if(/^IP_ADDRESS/) {
	@iplist = split(/\s+/,$_);
	$string = sprintf("IP_ADDRESS=\"10.0.0.%d\"", $id);
	s/$iplist[0]/$string/;
    }
    elsif(/^HOSTNAME/) {
	@iplist = split(/\s+/,$_);
	$string = sprintf("HOSTNAME=\"ct%d\"", $id);
	s/$iplist[0]/$string/;
    }
  } 
  untie @lines;

  `vzctl start $newve`;
  $newve++;
  $id++;
}
