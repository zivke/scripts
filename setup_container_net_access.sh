#!/bin/bash
# Create NAT routing from the internet to the containers
# Use CIDR notation for container addresses and command "$(hosname -i)" for
# host IP adrress
# Other solution: iptables -t nat -A POSTROUTING -s 10.0.0.0/20 -o eth0 -j SNAT --to $(hostname -i)
iptables -t nat -s 10.0.0.0/20 -A POSTROUTING -o eth0 -j MASQUERADE

# Forward everything coming from and going to the containers
# Affects containers with IP adresses in 10.0.0.0-10.0.15.255 range
iptables -A FORWARD -s 10.0.0.0/20 -j ACCEPT
iptables -A FORWARD -d 10.0.0.0/20 -j ACCEPT

# Save and restart iptables
service iptables save
service iptables restart
