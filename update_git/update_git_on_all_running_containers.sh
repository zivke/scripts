#!/bin/bash
# Update git on all existing containers (Ubuntu 12.04)

CONTAINERS=`vzlist -a | sed '1d' | awk '{print $1}'`
for C in $CONTAINERS; do
	echo Updating git on container $C
	SOURCES=/vz/private/$C/etc/apt/sources.list
	printf "deb http://ppa.launchpad.net/git-core/ppa/ubuntu precise main\ndeb-src http://ppa.launchpad.net/git-core/ppa/ubuntu precise main\n" >> $SOURCES
	cp pgp_key /vz/private/$C/tmp
	vzctl exec $C "apt-key add /tmp/pgp_key && apt-get update && apt-get -y upgrade git"
done