#!/bin/bash

if [ $# -eq 0 ]
then
	echo "Add commands to be executed at the end of the script call (e.g. sudo ./exec_in_running_containers.sh \"uptime\")."
	exit 1
fi

containers=`vzlist | sed '1d' | awk '{print $1}'`
for c in $containers; do
  vzctl exec $c $@
done