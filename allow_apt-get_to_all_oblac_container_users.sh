#!/bin/bash

CONTAINERS=`vzlist -a | sed '1d' | awk '{print $1}'`
for C in $CONTAINERS; do
	SUDOERS=/vz/private/$C/etc/sudoers
	echo >> $SUDOERS
	echo 'oblac ALL=(ALL)NOPASSWD:/usr/bin/apt-get' >> $SUDOERS
done
