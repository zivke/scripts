#!/bin/bash
# Take a (text) list of all workspaces from the database (id and name) and rename the directories from their id names to their true names

if [ $# -eq 0 ]
then
	echo "A text file with all ids and names of workspaces from the database is needed as input."
	# Run "mysql -u root -p --execute="SELECT id,name FROM oblac_development.workspaces" --skip-column-names -s > workspaces.txt" on the web server 
	# Output:
	# 1 first name
	# 2 second name
	exit 1
fi

declare -A WORKSPACE_MAP

while read line;
do
  ID=`echo $line | awk '{print $1}'`
  NAME=`echo $line | awk '{for (i=2; i<NF; i++) printf $i " "; print $NF}'`
  WORKSPACE_MAP[$ID]=$NAME
done < $1

CONTAINERS=`vzlist | sed '1d' | awk '{print $1}'`
for C in $CONTAINERS; do
  WORKSPACES=`ls /vz/private/$C/home/oblac/workspaces`
  for DIR in $WORKSPACES;
  do
    if [[ ${WORKSPACE_MAP[$DIR]} ]];
    then
      NAME=${WORKSPACE_MAP[$DIR]}
	  mv "/vz/private/${C}/home/oblac/workspaces/${DIR}" "/vz/private/${C}/home/oblac/workspaces/${NAME}"
    fi
  done
done

