#!/bin/bash

containers=`vzlist -a | sed '1d' | awk '{print $1}'`
for c in $containers; do
  vzctl start $c
done