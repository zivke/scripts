#!/bin/bash
# Reroute all git requests (that are sent to Synapticon GitHub repositories) to local repository mirrors

CONTAINERS=`vzlist | sed '1d' | awk '{print $1}'`
for C in $CONTAINERS; do
	GITCONFIG=/vz/private/$C/etc/gitconfig
	printf "[url \"/opt/oblac/software_components/\"]\n\tinsteadOf = git://github.com/synapticon/\n" > $GITCONFIG
done
