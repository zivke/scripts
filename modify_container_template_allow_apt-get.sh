#!/bin/bash

cd /vz/template/cache/
cp ubuntu-12.04-x86_64-oblac-latest.tar.gz ubuntu-12.04-x86_64-oblac-previous.tar.gz
mkdir tmp
tar -xzf ubuntu-12.04-x86_64-oblac-latest.tar.gz -C tmp/
rm ubuntu-12.04-x86_64-oblac-latest.tar.gz
echo >> tmp/etc/sudoers
echo 'oblac ALL=(ALL)NOPASSWD:/usr/bin/apt-get' >> tmp/etc/sudoers
tar -czf ubuntu-12.04-x86_64-oblac-latest.tar.gz -C tmp/ .
rm -rf tmp/