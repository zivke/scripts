#!/bin/bash
# Build the OS X version of the SOMANETconnect

export PATH=$PATH:/root/osxcross/target/bin

########## XSCOPE HOST JNI LIBRARY ##########
cd /root/xscope_host_jni_library/

# Remove previously built target
if [ -d target ]; then
  rm -r target
fi

# Fetch the newest xscope_host_jni_library master branch
git pull origin master

mkdir target

# Build xscope_host_jni_library
x86_64-apple-darwin15-cc -c -w -I/root/Java/OSX/jdk1.8.0_77.jdk/Contents/Home/include/ -I/root/Java/OSX/jdk1.8.0_77.jdk/Contents/Home/include/darwin/ -I/root/XMOS_essential/OSX/14.1.2/include/ SOMANETconnect_XscopeSocket.c -o target/SOMANETconnect_XscopeSocket.o

x86_64-apple-darwin15-cc -c -w -I/root/Java/OSX/jdk1.8.0_77.jdk/Contents/Home/include/ -I/root/Java/OSX/jdk1.8.0_77.jdk/Contents/Home/include/darwin/ -I/root/XMOS_essential/OSX/14.1.2/include/ callbacks.c -o target/callbacks.o

cd target

# Link the library
x86_64-apple-darwin15-cc -dynamiclib -w -o libXscopeSocket.jnilib SOMANETconnect_XscopeSocket.o callbacks.o -framework JavaVM -L/root/XMOS_essential/OSX/14.1.2/lib -lxscope_endpoint

# Change the path to the xscope_endpoint.so file dependency
x86_64-apple-darwin15-install_name_tool -change "xscope_endpoint.so" "@loader_path/libxscope_endpoint.dylib" libXscopeSocket.jnilib

# Copy the SOMANETconnect template
cd /root
if [ -d build_osx_tmp ]; then
  rm -r build_osx_tmp
fi

mkdir build_osx_tmp

cp -r /root/somanet_connect_templates/OSX/SOMANETconnect/ build_osx_tmp/

# Copy XMOS essential files
cp -r /root/XMOS_essential/OSX/14.1.2/* build_osx_tmp/SOMANETconnect/SOMANETconnect.app/Contents/Resources/

# Copy the xSCOPE endpoint host library
cp /root/xscope_host_jni_library/target/libXscopeSocket.jnilib build_osx_tmp/SOMANETconnect/SOMANETconnect.app/Contents/Resources/lib/

# Clean the built xSCOPE endpoint host library
rm -rf /root/xscope_host_jni_library/target

cd /root/SOMANETconnect
git checkout develop
git pull origin develop
mvn clean compile assembly:single

cp target/SOMANETconnect-*-jar-with-dependencies.jar  /root/build_osx_tmp/SOMANETconnect/SOMANETconnect.app/Contents/Resources/SOMANETconnect.jar

mvn clean

# Create a ZIP file
cd /root/build_osx_tmp/
zip -r /root/SOMANETconnect_OSX.zip SOMANETconnect/

rm -rf /root/build_osx_tmp

