#!/bin/bash
# Build the Windows version of the SOMANETconnect
########## XSCOPE HOST JNI LIBRARY ##########
cd /root/xscope_host_jni_library/

# Remove previously built target
if [ -d target ]; then
  rm -r target
fi

# Fetch the newest xscope_host_jni_library master branch
git pull origin master

mkdir target

# Build xscope_host_jni_library
#i686-w64-mingw32-gcc -c -w -I/usr/lib/jvm/java-7-openjdk-amd64/include -I/root/XMOS_essential/Windows/14.1.2/include/ SOMANETconnect_XscopeSocket.c -o target/SOMANETconnect_XscopeSocket.o
i686-w64-mingw32-gcc -c -w -I/root/Java/Windows/jdk1.7.0_51/include/ -I/root/Java/Windows/jdk1.7.0_51/include/win32/ -I/root/XMOS_essential/Windows/14.1.2/include/ SOMANETconnect_XscopeSocket.c -o target/SOMANETconnect_XscopeSocket.o

#i686-w64-mingw32-gcc -c -w -I/usr/lib/jvm/java-7-openjdk-amd64/include -I/root/XMOS_essential/Windows/14.1.2/include/ callbacks.c -o target/callbacks.o
i686-w64-mingw32-gcc -c -w -I/root/Java/Windows/jdk1.7.0_51/include/ -I/root/Java/Windows/jdk1.7.0_51/include/win32/ -I/root/XMOS_essential/Windows/14.1.2/include/ callbacks.c -o target/callbacks.o

cd target

# Link the library
i686-w64-mingw32-gcc -shared -o XscopeSocket.dll SOMANETconnect_XscopeSocket.o callbacks.o -L/root/XMOS_essential/Windows/14.1.2/lib -lxscope_endpoint -static-libgcc -Wl,--add-stdcall-alias

# Copy the SOMANETconnect template
cd /root
if [ -d build_win_tmp ]; then
  rm -r build_win_tmp
fi

mkdir build_win_tmp

cp -r /root/somanet_connect_templates/Windows/SOMANETconnect/ build_win_tmp/

# Copy XMOS essential files
cp -r /root/XMOS_essential/Windows/14.1.2/* build_win_tmp/SOMANETconnect

# Copy the xSCOPE endpoint host library
cp /root/xscope_host_jni_library/target/XscopeSocket.dll build_win_tmp/SOMANETconnect/lib/

# Clean the built xSCOPE endpoint host library
rm -rf /root/xscope_host_jni_library/target

cd /root/SOMANETconnect
git checkout develop
git pull origin develop
mvn clean compile assembly:single

cp target/SOMANETconnect-*-jar-with-dependencies.jar  /root/build_win_tmp/SOMANETconnect/SOMANETconnect.jar

mvn clean

# Create a ZIP file
cd /root/build_win_tmp/
zip -r /root/SOMANETconnect_win.zip SOMANETconnect/

rm -rf /root/build_win_tmp

