#!/bin/bash
# Build the Linux version of the SOMANETconnect
########## XSCOPE HOST JNI LIBRARY ##########
cd /root/xscope_host_jni_library/

# Remove previously built target
if [ -d target ]; then
  rm -r target
fi

# Fetch the newest xscope_host_jni_library master branch
git pull origin master

mkdir target

# Build xscope_host_jni_library
gcc -c -w -fPIC -I/usr/lib/jvm/java-7-openjdk-amd64/include -I/root/XMOS_essential/Linux/14.1.2/include/ SOMANETconnect_XscopeSocket.c -o target/SOMANETconnect_XscopeSocket.o

gcc -c -w -fPIC -I/usr/lib/jvm/java-7-openjdk-amd64/include -I/root/XMOS_essential/Linux/14.1.2/include/ callbacks.c -o target/callbacks.o

cd target

# Link the library
gcc -shared -o libXscopeSocket.so SOMANETconnect_XscopeSocket.o callbacks.o -L/root/XMOS_essential/Linux/14.1.2/lib -lxscope_endpoint -static-libgcc #-Wl,--add-stdcall-alias

# Copy the SOMANETconnect template
cd /root
if [ -d build_linux_tmp ]; then
  rm -r build_linux_tmp
fi

mkdir build_linux_tmp

cp -r /root/somanet_connect_templates/Linux/SOMANETconnect/ build_linux_tmp/

# Copy XMOS essential files
cp -r /root/XMOS_essential/Linux/14.1.2/* build_linux_tmp/SOMANETconnect

# Copy the xSCOPE endpoint host library
cp /root/xscope_host_jni_library/target/libXscopeSocket.so build_linux_tmp/SOMANETconnect/lib/

# Clean the built xSCOPE endpoint host library
rm -rf /root/xscope_host_jni_library/target

cd /root/SOMANETconnect
git checkout develop
git pull origin develop
mvn clean compile assembly:single

cp target/SOMANETconnect-*-jar-with-dependencies.jar  /root/build_linux_tmp/SOMANETconnect/SOMANETconnect.jar

mvn clean

# Create a ZIP file
cd /root/build_linux_tmp/
zip -r /root/SOMANETconnect_linux.zip SOMANETconnect/

rm -rf /root/build_linux_tmp

