#!/bin/bash

CONTAINERS=`vzlist -a | sed '1d' | awk '{print $1}'`
for C in $CONTAINERS; do
  vzctl stop $C
done