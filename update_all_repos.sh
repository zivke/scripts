#!/bin/bash
# Update all git mirror repositories in the current directory

# Ensure that this script is always started in the directory where it is located
cd "$( dirname "$0" )"

ALL_DIRS=`ls`

for DIR in $ALL_DIRS; do
	if [[ $DIR == *.git ]];
	then
		cd $DIR && git fetch -q --all && cd ..;
	fi
done
