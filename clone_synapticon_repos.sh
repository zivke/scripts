#!/bin/bash
# Clone all Synapticon software component repositories from GitHub as mirrors

# Ensure that this script is always started in the directory where it is located
cd "$( dirname "$0" )"

REPOLIST=`curl --silent https://api.github.com/orgs/synapticon/repos -q | grep "\"clone_url\"" | awk -F': "' '{print $2}' | sed -e 's/",//g'`

for REPO in $REPOLIST; do
	if [[ $REPO == */sc_*.git ]] || [[ $REPO == */sw_*.git ]];
	then
		git clone --mirror --recursive $REPO;
	fi
done
